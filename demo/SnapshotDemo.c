//
// SnapshotDemo is a brief example to show how to interpret data 
// read back from the Snapshot command.
// 
// To further process numerics such as voltage/iout/temperature
// into a readable floating value, use the pmbus numeric conversions 
// library at: https://bitbucket.org/ericssonpowermodules/c-pmbus-numeric-conversions
//

// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include <stdint.h>
#include <stdio.h>

#include "../src/SnapshotData.h"
#include "../src/StatusBitmasks.h"

void BMR465XX10_processStatusInput(uint8_t statusInput);
void BMR464XXX8_processStatusInput(uint8_t statusInput);
void BMR464SnapshotSample();
void BMR465SnapshotSample();

int main()
{
  BMR464SnapshotSample();
  BMR465SnapshotSample();

  return 0;
}

void BMR464SnapshotSample()
{
  //sample bytes assuming they are read directly back from the smbus transaction
  //such that byte 0 is snapshotSampleReadback[0]
  uint8_t snapshotSampleReadback[32] = { 0x20, 0xD3, //bytes 1:0 - input voltage - 12.5 
                0x9A, 0x69, //bytes 3:2 - output voltage - 3.3 (assume -13 vout_mode linear exp)
                0x00, 0xBB, //bytes 5:4 - load current - 
                0x00, 0xC2, //bytes 7:6 - peak current 
                0x00, 0x00, //bytes 9:8 - duty cycle
                0x20, 0xDB, //bytes 11:10 - internal temperature
                0x00, 0x00, //bytes 13:12 - reserved 
                0x80, 0xFA, //bytes 15:14 - switching frequency
                0x00,        //byte 16 - status_vout 
                0x00,        //byte 17 - status_iout 
                0xC0,        //byte 18 - status_input 
                0x00,        //byte 19 - status_temperature 
                0x00,        //byte 20 - status_cml 
                0x00,        //byte 21 - status_mfr
                0x00, 0x00, //bytes 31:22 - reserved 
                0x00, 0x00, 
                0x00, 0x00,
                0x00, 0x00, 
                0x00, 0x00};

  BMR464XXX8_SnapshotData snapshotData;

  BMR464XXX8_SnapshotBytesToData(&snapshotSampleReadback[0], &snapshotData);

  printf( "Input voltage (hex): %02X \n", snapshotData.InputVoltage);
  BMR464XXX8_processStatusInput(snapshotData.StatusInput);
}


void BMR465SnapshotSample()
{
  //sample bytes assuming they are read directly back from the smbus transaction
  //such that byte 0 is snapshotSampleReadback[0]
  uint8_t snapshotSampleReadback[32] = { 0x20, 0xD3, //bytes 1:0 - input voltage - 12.5 
                0x9A, 0x69, //bytes 3:2 - output voltage - 3.3 (assume -13 vout_mode linear exp)
                0x00, 0xBB, //bytes 5:4 - load current - 
                0x00, 0xC2, //bytes 7:6 - peak current 
                0x00, 0x00, //bytes 9:8 - duty cycle
                0x20, 0xDB, //bytes 11:10 - internal temperature
                0x00, 0x00, //bytes 13:12 - reserved 
                0x80, 0xFA, //bytes 15:14 - switching frequency
                0x00,        //byte 16 - status_vout 
                0x00,        //byte 17 - status_iout 
                0xC0,        //byte 18 - status_input 
                0x00,        //byte 19 - status_temperature 
                0x00,        //byte 20 - status_cml 
                0x00,        //byte 21 - status_mfr
                0x00,        //byte 22 - status_nvm 
                0x00, 0x00, //bytes 24:23 - reserved
                0x00, 0x00, //bytes 26:25 Load current phase 0 
                0x00, 0x00, //bytes 28:27 Load current phase 1
                0x00, 0x00, 0x00}; // bytes 31:29 - reserved

  BMR465XX10_SnapshotData snapshotData;

  BMR465XX10_SnapshotBytesToData(&snapshotSampleReadback[0], &snapshotData);

  printf( "Input voltage (hex): %02X \n", snapshotData.InputVoltage);
  BMR465XX10_processStatusInput(snapshotData.StatusInput);
}

void BMR464XXX8_processStatusInput(uint8_t statusInput)
{

  if(statusInput & BMR464XXX8_StatusInput_bVInUVFault)
    printf("VIn UV Fault \n");

  if(statusInput & BMR464XXX8_StatusInput_bVInUVWarning)
    printf("VIn UV Warning \n");

  if(statusInput & BMR464XXX8_StatusInput_bVInOVWarning)
    printf("VIn OV Warning \n");

  if(statusInput & BMR464XXX8_StatusInput_bVInOVFault)
    printf("VIn OV Fault \n");

}

void BMR465XX10_processStatusInput(uint8_t statusInput)
{

  if(statusInput & BMR465XX10_StatusInput_bVInUVFault)
    printf("VIn UV Fault \n");

  if(statusInput & BMR465XX10_StatusInput_bVInUVWarning)
    printf("VIn UV Warning \n");

  if(statusInput & BMR465XX10_StatusInput_bVInOVWarning)
    printf("VIn OV Warning \n");

  if(statusInput & BMR465XX10_StatusInput_bVInOVFault)
    printf("VIn OV Fault \n");

}



