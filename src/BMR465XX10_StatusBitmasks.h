// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#ifndef BMR465XX10_STATUSBITMASKS_H
#define BMR465XX10_STATUSBITMASKS_H

#include "PMBusSpec_StatusBitmasks.h"
#include "POLS4009_StatusMFRBitmasks.h"

#define BMR465XX10_StatusMFR_bVMONOVFault              POLS4009_StatusMFR_bVMONOVFault
#define BMR465XX10_StatusMFR_bVMONUVFault              POLS4009_StatusMFR_bVMONUVFault
#define BMR465XX10_StatusMFR_bGroupFault               POLS4009_StatusMFR_bGroupFault
#define BMR465XX10_StatusMFR_bLossOfExternalSyncFault  POLS4009_StatusMFR_bLossOfExternalSyncFault
#define BMR465XX10_StatusMFR_bVMONOVWarning            POLS4009_StatusMFR_bVMONOVWarning
#define BMR465XX10_StatusMFR_bVMONUVWarning            POLS4009_StatusMFR_bVMONUVWarning
#define BMR465XX10_StatusMFR_bGCBFault                 POLS4009_StatusMFR_bGCBFault
#define BMR465XX10_StatusMFR_bPhaseFault               POLS4009_StatusMFR_bPhaseFault

// #define BMR465XX10_StatusIOut_bPOutOPWarning     PMBusSpec_StatusIOut_bPOutOPWarning     
// #define BMR465XX10_StatusIOut_bPOutOPFault       PMBusSpec_StatusIOut_bPOutOPFault       
#define BMR465XX10_StatusIOut_bPhase1Fault	        POLS4009_StatusIOut_bPhase1Fault
#define BMR465XX10_StatusIOut_bPhase0Fault	        POLS4009_StatusIOut_bPhase0Fault
#define BMR465XX10_StatusIOut_bIOutUCFault	        POLS4009_StatusIOut_bIOutUCFault
// #define BMR465XX10_StatusIOut_bIOutOCWarning     PMBusSpec_StatusIOut_bIOutOCWarning     
// #define BMR465XX10_StatusIOut_bIOutOCLVFault     PMBusSpec_StatusIOut_bIOutOCLVFault     
#define BMR465XX10_StatusIOut_bIOutOCFault	        POLS4009_StatusIOut_bIOutOCFault

// #define BMR465XX10_StatusVOut_bVOutTrackingError    PMBusSpec_StatusVOut_bVOutTrackingError 
// #define BMR465XX10_StatusVOut_bTOffMaxWarning       PMBusSpec_StatusVOut_bTOffMaxWarning    
// #define BMR465XX10_StatusVOut_bTOnMaxFault          PMBusSpec_StatusVOut_bTOnMaxFault       
// #define BMR465XX10_StatusVOut_bVOutMaxWarning       PMBusSpec_StatusVOut_bVOutMaxWarning    
#define BMR465XX10_StatusVOut_bVOutUVFault          PMBusSpec_StatusVOut_bVOutUVFault       
// #define BMR465XX10_StatusVOut_bVOutUVWarning        PMBusSpec_StatusVOut_bVOutUVWarning     
// #define BMR465XX10_StatusVOut_bVOutOVWarning        PMBusSpec_StatusVOut_bVOutOVWarning     
#define BMR465XX10_StatusVOut_bVOutOVFault          PMBusSpec_StatusVOut_bVOutOVFault       

// #define BMR465XX10_StatusInput_bPInOPWarning        PMBusSpec_StatusInput_bPInOPWarning         
// #define BMR465XX10_StatusInput_bIInOCWarning        PMBusSpec_StatusInput_bIInOCWarning         
// #define BMR465XX10_StatusInput_bIInOCFault          PMBusSpec_StatusInput_bIInOCFault           
// #define BMR465XX10_StatusInput_bUnitOffForLowInputVoltage  PMBusSpec_StatusInput_bUnitOffForLowInputVoltage    
#define BMR465XX10_StatusInput_bVInUVFault          PMBusSpec_StatusInput_bVInUVFault           
#define BMR465XX10_StatusInput_bVInUVWarning        PMBusSpec_StatusInput_bVInUVWarning         
#define BMR465XX10_StatusInput_bVInOVWarning        PMBusSpec_StatusInput_bVInOVWarning         
#define BMR465XX10_StatusInput_bVInOVFault          PMBusSpec_StatusInput_bVInOVFault           

// #define BMR465XX10_StatusTemperature_bReserved0     PMBusSpec_StatusTemperature_bReserved0  
// #define BMR465XX10_StatusTemperature_bReserved1     PMBusSpec_StatusTemperature_bReserved1  
// #define BMR465XX10_StatusTemperature_bReserved2     PMBusSpec_StatusTemperature_bReserved2  
#define BMR465XX10_StatusTemperature_bInternalTempSensorFault		0x08
#define BMR465XX10_StatusTemperature_bUTFault       PMBusSpec_StatusTemperature_bUTFault        
#define BMR465XX10_StatusTemperature_bUTWarning     PMBusSpec_StatusTemperature_bUTWarning  
#define BMR465XX10_StatusTemperature_bOTWarning     PMBusSpec_StatusTemperature_bOTWarning  
#define BMR465XX10_StatusTemperature_bOTFault       PMBusSpec_StatusTemperature_bOTFault        

// #define BMR465XX10_StatusCML_bOtherMemoryOrLogicFault       PMBusSpec_StatusCML_bOtherMemoryOrLogicFault        
#define BMR465XX10_StatusCML_bOtherCommunicationFault       PMBusSpec_StatusCML_bOtherCommunicationFault        
// #define BMR465XX10_StatusCML_bReserved2                     PMBusSpec_StatusCML_bReserved2                  
// #define BMR465XX10_StatusCML_bProcessorFaultDetected        PMBusSpec_StatusCML_bProcessorFaultDetected     
// #define BMR465XX10_StatusCML_bMemoryFaultDetected           PMBusSpec_StatusCML_bMemoryFaultDetected            
#define BMR465XX10_StatusCML_bPacketErrorCheckFailed        PMBusSpec_StatusCML_bPacketErrorCheckFailed     
#define BMR465XX10_StatusCML_bInvalidOrUnsupportedData      PMBusSpec_StatusCML_bInvalidOrUnsupportedData       
#define BMR465XX10_StatusCML_bInvalidOrUnsupportedCommand   PMBusSpec_StatusCML_bInvalidOrUnsupportedCommand    


#endif
