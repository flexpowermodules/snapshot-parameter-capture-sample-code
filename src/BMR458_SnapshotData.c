// Copyright © Ericsson AB 2016

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "BMR458_SnapshotData.h"

void BMR458_SnapshotBytesToData(uint8_t *snapshotBytes, BMR458_SnapshotData *snapshotData)
{
  //NOTE: field is big-endian format
  snapshotData->OldInputVoltage = (((uint16_t) snapshotBytes[0] << 8) | 0x00FF) & 
                                  (((uint16_t) snapshotBytes[1]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->OldOutputVoltage = (((uint16_t) snapshotBytes[2] << 8) | 0x00FF) & 
                                   (((uint16_t) snapshotBytes[3]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->OldLoadCurrent = (((uint16_t) snapshotBytes[4] << 8) | 0x00FF) & 
                                 (((uint16_t) snapshotBytes[5]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->OldDutyCycle = (((uint16_t) snapshotBytes[6] << 8) | 0x00FF) & 
                               (((uint16_t) snapshotBytes[7]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->InputVoltage = (((uint16_t) snapshotBytes[8] << 8) | 0x00FF) & 
                               (((uint16_t) snapshotBytes[9]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->OutputVoltage = (((uint16_t) snapshotBytes[10] << 8) | 0x00FF) & 
                                (((uint16_t) snapshotBytes[11]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->LoadCurrent = (((uint16_t) snapshotBytes[12] << 8) | 0x00FF) & 
                              (((uint16_t) snapshotBytes[13]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->TemperatureOne = (((uint16_t) snapshotBytes[14] << 8) | 0x00FF) & 
                                 (((uint16_t) snapshotBytes[15]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->TemperatureTwo = (((uint16_t) snapshotBytes[16] << 8) | 0x00FF) & 
                                 (((uint16_t) snapshotBytes[17]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->TimeInOperation = (((uint16_t) snapshotBytes[18] << 8) | 0x00FF) & 
                                  (((uint16_t) snapshotBytes[19]) | 0xFF00);

  //NOTE: field is big-endian format
  snapshotData->StatusWord = (((uint16_t) snapshotBytes[20] << 8) | 0x00FF) & 
                             (((uint16_t) snapshotBytes[21]) | 0xFF00);

  snapshotData->StatusByte = snapshotBytes[22];
  snapshotData->StatusVOut = snapshotBytes[23];
  snapshotData->StatusIOut = snapshotBytes[24];
  snapshotData->StatusInput = snapshotBytes[25];
  snapshotData->StatusTemperature = snapshotBytes[26];
  snapshotData->StatusCML = snapshotBytes[27];
  snapshotData->StatusOther = snapshotBytes[28];
  snapshotData->StatusMFR = snapshotBytes[29];

  //NOTE: field is big-endian format
  snapshotData->SnapshotCycles = (((uint16_t) snapshotBytes[30] << 8) | 0x00FF) & 
                                 (((uint16_t) snapshotBytes[31]) | 0xFF00);
}
