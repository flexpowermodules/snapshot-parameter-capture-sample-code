// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef SNAPSHOT_DATA_H
#define SNAPSHOT_DATA_H

#include "BMR458_SnapshotData.h"
#include "POL2008E_SnapshotData.h"
#include "POLS1003_SnapshotData.h"
#include "POLS4009_SnapshotData.h"

typedef POL2008E_SnapshotData BMR462_SnapshotData;

typedef POL2008E_SnapshotData BMR463XXX2_SnapshotData;
typedef POL2008E_SnapshotData BMR463XXX6_SnapshotData;
typedef POLS1003_SnapshotData BMR463XXX8_SnapshotData;
typedef POLS1003_SnapshotData BMR463XXX9_SnapshotData;

typedef POL2008E_SnapshotData BMR464XXX2_SnapshotData;
typedef POL2008E_SnapshotData BMR464XXX6_SnapshotData;
typedef POLS1003_SnapshotData BMR464XXX8_SnapshotData;
typedef POLS1003_SnapshotData BMR464XXX9_SnapshotData;

typedef POLS4009_SnapshotData BMR465XX10_SnapshotData;

#define BMR462_SnapshotBytesToData(snapshotBytes, snapshotData) POL2008E_SnapshotBytesToData(snapshotBytes, snapshotData)

#define BMR463XXX2_SnapshotBytesToData(snapshotBytes, snapshotData) POL2008E_SnapshotBytesToData(snapshotBytes, snapshotData)
#define BMR463XXX6_SnapshotBytesToData(snapshotBytes, snapshotData) POL2008E_SnapshotBytesToData(snapshotBytes, snapshotData)
#define BMR463XXX8_SnapshotBytesToData(snapshotBytes, snapshotData) POLS1003_SnapshotBytesToData(snapshotBytes, snapshotData)
#define BMR463XXX9_SnapshotBytesToData(snapshotBytes, snapshotData) POLS1003_SnapshotBytesToData(snapshotBytes, snapshotData)

#define BMR464XXX2_SnapshotBytesToData(snapshotBytes, snapshotData) POL2008E_SnapshotBytesToData(snapshotBytes, snapshotData)
#define BMR464XXX6_SnapshotBytesToData(snapshotBytes, snapshotData) POL2008E_SnapshotBytesToData(snapshotBytes, snapshotData)
#define BMR464XXX8_SnapshotBytesToData(snapshotBytes, snapshotData) POLS1003_SnapshotBytesToData(snapshotBytes, snapshotData)
#define BMR464XXX9_SnapshotBytesToData(snapshotBytes, snapshotData) POLS1003_SnapshotBytesToData(snapshotBytes, snapshotData)

#define BMR465XX10_SnapshotBytesToData(snapshotBytes, snapshotData) POLS4009_SnapshotBytesToData(snapshotBytes, snapshotData)

#endif

