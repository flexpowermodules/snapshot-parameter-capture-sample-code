// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#ifndef BMR464XXX9_STATUSBITMASKS_H
#define BMR464XXX9_STATUSBITMASKS_H

#include "PMBusSpec_StatusBitmasks.h"
#include "POLS1003_StatusMFRBitmasks.h"

#define BMR464XXX9_StatusMFR_bVMONOVFault              POLS1003_StatusMFR_bVMONOVFault            
#define BMR464XXX9_StatusMFR_bVMONUVFault              POLS1003_StatusMFR_bVMONUVFault            
#define BMR464XXX9_StatusMFR_bReserved2                POLS1003_StatusMFR_bReserved2              
#define BMR464XXX9_StatusMFR_bLossOfExternalSyncFault  POLS1003_StatusMFR_bLossOfExternalSyncFault
#define BMR464XXX9_StatusMFR_bVMONOVWarning            POLS1003_StatusMFR_bVMONOVWarning          
#define BMR464XXX9_StatusMFR_bVMONUVWarning            POLS1003_StatusMFR_bVMONUVWarning          
#define BMR464XXX9_StatusMFR_bReserved6                POLS1003_StatusMFR_bReserved6              
#define BMR464XXX9_StatusMFR_bReserved7                POLS1003_StatusMFR_bReserved7              

// #define BMR464XXX9_StatusVOut_bVOutTrackingError    StatusVOut_PMBusSpec_bVOutTrackingError 
#define BMR464XXX9_StatusVOut_bTOffMaxWarning       StatusVOut_PMBusSpec_bTOffMaxWarning    
#define BMR464XXX9_StatusVOut_bTOnMaxFault          StatusVOut_PMBusSpec_bTOnMaxFault       
#define BMR464XXX9_StatusVOut_bVOutMaxWarning       StatusVOut_PMBusSpec_bVOutMaxWarning    
#define BMR464XXX9_StatusVOut_bVOutUVFault          StatusVOut_PMBusSpec_bVOutUVFault       
// #define BMR464XXX9_StatusVOut_bVOutUVWarning        StatusVOut_PMBusSpec_bVOutUVWarning     
// #define BMR464XXX9_StatusVOut_bVOutOVWarning        StatusVOut_PMBusSpec_bVOutOVWarning     
#define BMR464XXX9_StatusVOut_bVOutOVFault          StatusVOut_PMBusSpec_bVOutOVFault       

// #define BMR464XXX9_StatusIOut_bPOutOPWarning     StatusIOut_PMBusSpec_bPOutOPWarning     
// #define BMR464XXX9_StatusIOut_bPOutOPFault          StatusIOut_PMBusSpec_bPOutOPFault       
// #define BMR464XXX9_StatusIOut_bInPowerLimitingMode  StatusIOut_PMBusSpec_bInPowerLimitingMode   
// #define BMR464XXX9_StatusIOut_bCurrentShareFault    StatusIOut_PMBusSpec_bCurrentShareFault 
#define BMR464XXX9_StatusIOut_bIOutUCFault          StatusIOut_PMBusSpec_bIOutUCFault       
// #define BMR464XXX9_StatusIOut_bIOutOCWarning     StatusIOut_PMBusSpec_bIOutOCWarning     
// #define BMR464XXX9_StatusIOut_bIOutOCLVFault     StatusIOut_PMBusSpec_bIOutOCLVFault     
#define BMR464XXX9_StatusIOut_bIOutOCFault          StatusIOut_PMBusSpec_bIOutOCFault       

// #define BMR464XXX9_StatusInput_bPInOPWarning        StatusInput_PMBusSpec_bPInOPWarning         
// #define BMR464XXX9_StatusInput_bIInOCWarning        StatusInput_PMBusSpec_bIInOCWarning         
// #define BMR464XXX9_StatusInput_bIInOCFault          StatusInput_PMBusSpec_bIInOCFault           
// #define BMR464XXX9_StatusInput_bUnitOffForLowInputVoltage  StatusInput_PMBusSpec_bUnitOffForLowInputVoltage    
#define BMR464XXX9_StatusInput_bVInUVFault          StatusInput_PMBusSpec_bVInUVFault           
#define BMR464XXX9_StatusInput_bVInUVWarning        StatusInput_PMBusSpec_bVInUVWarning         
#define BMR464XXX9_StatusInput_bVInOVWarning        StatusInput_PMBusSpec_bVInOVWarning         
#define BMR464XXX9_StatusInput_bVInOVFault          StatusInput_PMBusSpec_bVInOVFault           

// #define BMR464XXX9_StatusTemperature_bReserved0     StatusTemperature_PMBusSpec_bReserved0  
// #define BMR464XXX9_StatusTemperature_bReserved1     StatusTemperature_PMBusSpec_bReserved1  
// #define BMR464XXX9_StatusTemperature_bReserved2     StatusTemperature_PMBusSpec_bReserved2  
// #define BMR464XXX9_StatusTemperature_bReserved3     StatusTemperature_PMBusSpec_bReserved3  
#define BMR464XXX9_StatusTemperature_bUTFault       StatusTemperature_PMBusSpec_bUTFault        
#define BMR464XXX9_StatusTemperature_bUTWarning     StatusTemperature_PMBusSpec_bUTWarning  
#define BMR464XXX9_StatusTemperature_bOTWarning     StatusTemperature_PMBusSpec_bOTWarning  
#define BMR464XXX9_StatusTemperature_bOTFault       StatusTemperature_PMBusSpec_bOTFault        

#define BMR464XXX9_StatusCML_bOtherMemoryOrLogicFault       StatusCML_PMBusSpec_bOtherMemoryOrLogicFault        
#define BMR464XXX9_StatusCML_bOtherCommunicationFault       StatusCML_PMBusSpec_bOtherCommunicationFault        
// #define BMR464XXX9_StatusCML_bReserved2                     StatusCML_PMBusSpec_bReserved2                  
#define BMR464XXX9_StatusCML_bProcessorFaultDetected        StatusCML_PMBusSpec_bProcessorFaultDetected     
#define BMR464XXX9_StatusCML_bMemoryFaultDetected           StatusCML_PMBusSpec_bMemoryFaultDetected            
#define BMR464XXX9_StatusCML_bPacketErrorCheckFailed        StatusCML_PMBusSpec_bPacketErrorCheckFailed     
#define BMR464XXX9_StatusCML_bInvalidOrUnsupportedData      StatusCML_PMBusSpec_bInvalidOrUnsupportedData       
#define BMR464XXX9_StatusCML_bInvalidOrUnsupportedCommand   StatusCML_PMBusSpec_bInvalidOrUnsupportedCommand    

#endif
