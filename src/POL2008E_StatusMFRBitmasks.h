// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef POL2008E_STATUSMFRBITMASKS_H
#define POL2008E_STATUSMFRBITMASKS_H

#define POL2008E_StatusMFR_bVMONOVFault              0x01
#define POL2008E_StatusMFR_bVMONUVFault              0x02
#define POL2008E_StatusMFR_bReserved2                0x04
#define POL2008E_StatusMFR_bLossOfExternalSyncFault  0x08
#define POL2008E_StatusMFR_bVMONOVWarning            0x10
#define POL2008E_StatusMFR_bVMONUVWarning            0x20
#define POL2008E_StatusMFR_bReserved6                0x40
#define POL2008E_StatusMFR_bReserved7                0x80

#endif
