
#define POLS4009_SnapshotFaultMask_VOutOVFaultMask            0x0001
#define POLS4009_SnapshotFaultMask_VOutUVFaultMask            0x0002
#define POLS4009_SnapshotFaultMask_TemperatureOTFaultMask     0x0004
#define POLS4009_SnapshotFaultMask_TemperatureUTFaultMask     0x0008
#define POLS4009_SnapshotFaultMask_VInOVFaultMask             0x0010
#define POLS4009_SnapshotFaultMask_VInUVFaultMask             0x0020
#define POLS4009_SnapshotFaultMask_IoutOCFaultMask            0x0040
#define POLS4009_SnapshotFaultMask_IoutUCFaultMask            0x0080
#define POLS4009_SnapshotFaultMask_VMONOVFaultMask            0x0100
#define POLS4009_SnapshotFaultMask_VMONUVFaultMask            0x0200
#define POLS4009_SnapshotFaultMask_PacketErrorCheckFaultMask  0x0400
#define POLS4009_SnapshotFaultMask_ProcessorFaultMask         0x0800
#define POLS4009_SnapshotFaultMask_PhaseFaultMask             0x1000
#define POLS4009_SnapshotFaultMask_RailInFaultGroupFaultMask  0x2000
