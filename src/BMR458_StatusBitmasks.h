// Copyright © Ericsson AB 2016

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Based on BMR458XXXX001R1.xml documentRevision 1.10.99

#ifndef BMR458_STATUSBITMASKS_H
#define BMR458_STATUSBITMASKS_H

#include "PMBusSpec_StatusBitmasks.h"

// #define BMR458_StatusVOut_bVOutTrackingError    PMBusSpec_StatusVOut_bVOutTrackingError
#define BMR458_StatusVOut_bTOffMaxWarning       PMBusSpec_StatusVOut_bTOffMaxWarning
#define BMR458_StatusVOut_bTOnMaxFault          PMBusSpec_StatusVOut_bTOnMaxFault
#define BMR458_StatusVOut_bVOutMaxWarning       PMBusSpec_StatusVOut_bVOutMaxWarning
#define BMR458_StatusVOut_bVOutUVFault          PMBusSpec_StatusVOut_bVOutUVFault
#define BMR458_StatusVOut_bVOutUVWarning        PMBusSpec_StatusVOut_bVOutUVWarning
#define BMR458_StatusVOut_bVOutOVWarning        PMBusSpec_StatusVOut_bVOutOVWarning
#define BMR458_StatusVOut_bVOutOVFault          PMBusSpec_StatusVOut_bVOutOVFault

// #define BMR458_StatusIOut_bPOutOPWarning     PMBusSpec_StatusIOut_bPOutOPWarning
// #define BMR458_StatusIOut_bPOutOPFault          PMBusSpec_StatusIOut_bPOutOPFault
// #define BMR458_StatusIOut_bInPowerLimitingMode  PMBusSpec_StatusIOut_bInPowerLimitingMode
// #define BMR458_StatusIOut_bCurrentShareFault    PMBusSpec_StatusIOut_bCurrentShareFault
#define BMR458_StatusIOut_bIOutUCFault          PMBusSpec_StatusIOut_bIOutUCFault
#define BMR458_StatusIOut_bIOutOCWarning     PMBusSpec_StatusIOut_bIOutOCWarning
#define BMR458_StatusIOut_bIOutOCLVFault     PMBusSpec_StatusIOut_bIOutOCLVFault
#define BMR458_StatusIOut_bIOutOCFault          PMBusSpec_StatusIOut_bIOutOCFault

// #define BMR458_StatusInput_bPInOPWarning        PMBusSpec_StatusInput_bPInOPWarning
// #define BMR458_StatusInput_bIInOCWarning        PMBusSpec_StatusInput_bIInOCWarning
// #define BMR458_StatusInput_bIInOCFault          PMBusSpec_StatusInput_bIInOCFault
#define BMR458_StatusInput_bUnitOffForLowInputVoltage  PMBusSpec_StatusInput_bUnitOffForLowInputVoltage
#define BMR458_StatusInput_bVInUVFault          PMBusSpec_StatusInput_bVInUVFault
#define BMR458_StatusInput_bVInUVWarning        PMBusSpec_StatusInput_bVInUVWarning
#define BMR458_StatusInput_bVInOVWarning        PMBusSpec_StatusInput_bVInOVWarning
#define BMR458_StatusInput_bVInOVFault          PMBusSpec_StatusInput_bVInOVFault

// #define BMR458_StatusTemperature_bReserved0     PMBusSpec_StatusTemperature_bReserved0
// #define BMR458_StatusTemperature_bReserved1     PMBusSpec_StatusTemperature_bReserved1
// #define BMR458_StatusTemperature_bReserved2     PMBusSpec_StatusTemperature_bReserved2
// #define BMR458_StatusTemperature_bReserved3     PMBusSpec_StatusTemperature_bReserved3
#define BMR458_StatusTemperature_bUTFault       PMBusSpec_StatusTemperature_bUTFault
#define BMR458_StatusTemperature_bUTWarning     PMBusSpec_StatusTemperature_bUTWarning
#define BMR458_StatusTemperature_bOTWarning     PMBusSpec_StatusTemperature_bOTWarning
#define BMR458_StatusTemperature_bOTFault       PMBusSpec_StatusTemperature_bOTFault

// #define BMR458_StatusCML_bOtherMemoryOrLogicFault       PMBusSpec_StatusCML_bOtherMemoryOrLogicFault
#define BMR458_StatusCML_bOtherCommunicationFault       PMBusSpec_StatusCML_bOtherCommunicationFault
// #define BMR458_StatusCML_bReserved2                     PMBusSpec_StatusCML_bReserved2
// #define BMR458_StatusCML_bProcessorFaultDetected        PMBusSpec_StatusCML_bProcessorFaultDetected
#define BMR458_StatusCML_bMemoryFaultDetected           PMBusSpec_StatusCML_bMemoryFaultDetected
#define BMR458_StatusCML_bPacketErrorCheckFailed        PMBusSpec_StatusCML_bPacketErrorCheckFailed
#define BMR458_StatusCML_bInvalidOrUnsupportedData      PMBusSpec_StatusCML_bInvalidOrUnsupportedData
// #define BMR458_StatusCML_bInvalidOrUnsupportedCommand   PMBusSpec_StatusCML_bInvalidOrUnsupportedCommand


#endif
