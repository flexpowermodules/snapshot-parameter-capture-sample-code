// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <stdint.h>

typedef struct
{
  uint16_t InputVoltage;
  uint16_t OutputVoltage;
  uint16_t LoadCurrent;
  uint16_t PeakCurrent;
  uint16_t DutyCycle;
  uint16_t InternalTemperature;
  uint8_t ReservedBytes12_13[2];
  uint16_t SwitchingFrequency;
  uint8_t StatusVOut;
  uint8_t StatusIOut;
  uint8_t StatusInput;
  uint8_t StatusTemperature;
  uint8_t StatusCML;
  uint8_t StatusMFR;
  uint8_t StatusNVM;
  uint8_t ReservedBytes23_24[2];
  uint16_t LoadCurrentPhase0;
  uint16_t LoadCurrentPhase1;
  uint8_t ReservedBytes29_31[3];
} POLS4009_SnapshotData;


void POLS4009_SnapshotBytesToData(uint8_t *snapshotBytes, POLS4009_SnapshotData *snapshotData);
