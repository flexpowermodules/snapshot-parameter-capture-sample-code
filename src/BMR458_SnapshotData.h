// Copyright © Ericsson AB 2016

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef BMR458_SNAPSHOTDATA_H
#define BMR458_SNAPSHOTDATA_H

#include <stdint.h>

typedef struct 
{
  uint16_t OldInputVoltage;
  uint16_t OldOutputVoltage;
  uint16_t OldLoadCurrent;
  uint16_t OldDutyCycle;
  uint16_t InputVoltage;
  uint16_t OutputVoltage;
  uint16_t LoadCurrent;
  uint16_t TemperatureOne;
  uint16_t TemperatureTwo;
  uint16_t TimeInOperation;
  uint16_t StatusWord;
  uint8_t  StatusByte;
  uint8_t  StatusVOut;
  uint8_t  StatusIOut;
  uint8_t  StatusInput;  
  uint8_t  StatusTemperature;
  uint8_t  StatusCML;
  uint8_t  StatusOther;
  uint8_t  StatusMFR;
  uint16_t SnapshotCycles;
} BMR458_SnapshotData;


void BMR458_SnapshotBytesToData(uint8_t *snapshotBytes, BMR458_SnapshotData *snapshotData);

#endif