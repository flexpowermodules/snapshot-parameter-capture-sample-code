// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef POLS4009_STATUSMFRBITMASKS_H
#define POLS4009_STATUSMFRBITMASKS_H

#define POLS4009_StatusMFR_bVMONOVFault              0x01
#define POLS4009_StatusMFR_bVMONUVFault              0x02
#define POLS4009_StatusMFR_bGroupFault               0x04
#define POLS4009_StatusMFR_bLossOfExternalSyncFault  0x08
#define POLS4009_StatusMFR_bVMONOVWarning            0x10
#define POLS4009_StatusMFR_bVMONUVWarning            0x20
#define POLS4009_StatusMFR_bGCBFault                 0x40
#define POLS4009_StatusMFR_bPhaseFault               0x80

// #define POLS4009_StatusIOut_bPOutOPWarning     StatusIOut_PMBusSpec_bPOutOPWarning     
// #define POLS4009_StatusIOut_bPOutOPFault          StatusIOut_PMBusSpec_bPOutOPFault       
#define POLS4009_StatusIOut_bPhase1Fault            0x04 
#define POLS4009_StatusIOut_bPhase0Fault            0x08  
#define POLS4009_StatusIOut_bIOutUCFault            0x10       
// #define POLS4009_StatusIOut_bIOutOCWarning     StatusIOut_PMBusSpec_bIOutOCWarning     
// #define POLS4009_StatusIOut_bIOutOCLVFault     StatusIOut_PMBusSpec_bIOutOCLVFault     
#define POLS4009_StatusIOut_bIOutOCFault            0x80       

#endif
