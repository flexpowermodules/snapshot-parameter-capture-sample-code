// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "POL2008E_SnapshotData.h"

void POL2008E_SnapshotBytesToData(uint8_t *snapshotBytes, POL2008E_SnapshotData *snapshotData)
{
  int i;
  
  snapshotData->InputVoltage = (((uint16_t) snapshotBytes[1] << 8) | 0x00FF) & 
                                    (((uint16_t) snapshotBytes[0]) | 0xFF00); 

  snapshotData->OutputVoltage = (((uint16_t) snapshotBytes[3] << 8) | 0x00FF) & 
                                     (((uint16_t) snapshotBytes[2]) | 0xFF00); 

  snapshotData->LoadCurrent = (((uint16_t) snapshotBytes[5] << 8) | 0x00FF) & 
                                   (((uint16_t) snapshotBytes[4]) | 0xFF00); 

  snapshotData->PeakCurrent = (((uint16_t) snapshotBytes[7] << 8) | 0x00FF) & 
                                   (((uint16_t) snapshotBytes[6]) | 0xFF00); 

  snapshotData->DutyCycle = (((uint16_t) snapshotBytes[9] << 8) | 0x00FF) & 
                                 (((uint16_t) snapshotBytes[8]) | 0xFF00); 

  snapshotData->InternalTemperature = (((uint16_t) snapshotBytes[11] << 8) | 0x00FF) & 
                                           (((uint16_t) snapshotBytes[10]) | 0xFF00); 

  snapshotData->ReservedBytes12_13[0] = snapshotBytes[12];
  snapshotData->ReservedBytes12_13[1] = snapshotBytes[13];

  snapshotData->SwitchingFrequency = (((uint16_t) snapshotBytes[15] << 8) | 0x00FF) & 
                                          (((uint16_t) snapshotBytes[14]) | 0xFF00); 

  snapshotData->StatusVOut = snapshotBytes[16];
  snapshotData->StatusIOut = snapshotBytes[17];
  snapshotData->StatusInput = snapshotBytes[18];
  snapshotData->StatusTemperature = snapshotBytes[19];
  snapshotData->StatusCML = snapshotBytes[20];
  snapshotData->StatusMFR = snapshotBytes[21];

  for (i = 0; i < 10; i++)
  {
    snapshotData->ReservedBytes22_31[i] = snapshotBytes[i + 22]; 
  }

}

