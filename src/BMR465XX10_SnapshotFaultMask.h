
#include "POLS4009_SnapshotFaultMask.h"

#define BMR465XX10_SnapshotFaultMask_VOutOVFaultMask           POLS4009_SnapshotFaultMask_VOutOVFaultMask          
#define BMR465XX10_SnapshotFaultMask_VOutUVFaultMask           POLS4009_SnapshotFaultMask_VOutUVFaultMask          
#define BMR465XX10_SnapshotFaultMask_TemperatureOTFaultMask    POLS4009_SnapshotFaultMask_TemperatureOTFaultMask   
#define BMR465XX10_SnapshotFaultMask_TemperatureUTFaultMask    POLS4009_SnapshotFaultMask_TemperatureUTFaultMask   
#define BMR465XX10_SnapshotFaultMask_VInOVFaultMask            POLS4009_SnapshotFaultMask_VInOVFaultMask           
#define BMR465XX10_SnapshotFaultMask_VInUVFaultMask            POLS4009_SnapshotFaultMask_VInUVFaultMask           
#define BMR465XX10_SnapshotFaultMask_IoutOCFaultMask           POLS4009_SnapshotFaultMask_IoutOCFaultMask          
#define BMR465XX10_SnapshotFaultMask_IoutUCFaultMask           POLS4009_SnapshotFaultMask_IoutUCFaultMask          
#define BMR465XX10_SnapshotFaultMask_VMONOVFaultMask           POLS4009_SnapshotFaultMask_VMONOVFaultMask          
#define BMR465XX10_SnapshotFaultMask_VMONUVFaultMask           POLS4009_SnapshotFaultMask_VMONUVFaultMask          
#define BMR465XX10_SnapshotFaultMask_PacketErrorCheckFaultMask POLS4009_SnapshotFaultMask_PacketErrorCheckFaultMask
#define BMR465XX10_SnapshotFaultMask_ProcessorFaultMask        POLS4009_SnapshotFaultMask_ProcessorFaultMask       
#define BMR465XX10_SnapshotFaultMask_PhaseFaultMask            POLS4009_SnapshotFaultMask_PhaseFaultMask           
#define BMR465XX10_SnapshotFaultMask_RailInFaultGroupFaultMask POLS4009_SnapshotFaultMask_RailInFaultGroupFaultMask
