// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#ifndef BMR464XXX8_STATUSBITMASKS_H
#define BMR464XXX8_STATUSBITMASKS_H

#include "PMBusSpec_StatusBitmasks.h"
#include "POLS1003_StatusMFRBitmasks.h"

#define BMR464XXX8_StatusMFR_bVMONOVFault              POLS1003_StatusMFR_bVMONOVFault            
#define BMR464XXX8_StatusMFR_bVMONUVFault              POLS1003_StatusMFR_bVMONUVFault            
#define BMR464XXX8_StatusMFR_bReserved2                POLS1003_StatusMFR_bReserved2              
#define BMR464XXX8_StatusMFR_bLossOfExternalSyncFault  POLS1003_StatusMFR_bLossOfExternalSyncFault
#define BMR464XXX8_StatusMFR_bVMONOVWarning            POLS1003_StatusMFR_bVMONOVWarning          
#define BMR464XXX8_StatusMFR_bVMONUVWarning            POLS1003_StatusMFR_bVMONUVWarning          
#define BMR464XXX8_StatusMFR_bReserved6                POLS1003_StatusMFR_bReserved6              
#define BMR464XXX8_StatusMFR_bReserved7                POLS1003_StatusMFR_bReserved7              

// #define BMR464XXX8_StatusVOut_bVOutTrackingError    PMBusSpec_StatusVOut_bVOutTrackingError
#define BMR464XXX8_StatusVOut_bTOffMaxWarning       PMBusSpec_StatusVOut_bTOffMaxWarning
#define BMR464XXX8_StatusVOut_bTOnMaxFault          PMBusSpec_StatusVOut_bTOnMaxFault
#define BMR464XXX8_StatusVOut_bVOutMaxWarning       PMBusSpec_StatusVOut_bVOutMaxWarning
#define BMR464XXX8_StatusVOut_bVOutUVFault          PMBusSpec_StatusVOut_bVOutUVFault
// #define BMR464XXX8_StatusVOut_bVOutUVWarning        PMBusSpec_StatusVOut_bVOutUVWarning
// #define BMR464XXX8_StatusVOut_bVOutOVWarning        PMBusSpec_StatusVOut_bVOutOVWarning
#define BMR464XXX8_StatusVOut_bVOutOVFault          PMBusSpec_StatusVOut_bVOutOVFault

// #define BMR464XXX8_StatusIOut_bPOutOPWarning     PMBusSpec_StatusIOut_bPOutOPWarning
// #define BMR464XXX8_StatusIOut_bPOutOPFault          PMBusSpec_StatusIOut_bPOutOPFault
// #define BMR464XXX8_StatusIOut_bInPowerLimitingMode  PMBusSpec_StatusIOut_bInPowerLimitingMode
// #define BMR464XXX8_StatusIOut_bCurrentShareFault    PMBusSpec_StatusIOut_bCurrentShareFault
#define BMR464XXX8_StatusIOut_bIOutUCFault          PMBusSpec_StatusIOut_bIOutUCFault
// #define BMR464XXX8_StatusIOut_bIOutOCWarning     PMBusSpec_StatusIOut_bIOutOCWarning
// #define BMR464XXX8_StatusIOut_bIOutOCLVFault     PMBusSpec_StatusIOut_bIOutOCLVFault
#define BMR464XXX8_StatusIOut_bIOutOCFault          PMBusSpec_StatusIOut_bIOutOCFault

// #define BMR464XXX8_StatusInput_bPInOPWarning        PMBusSpec_StatusInput_bPInOPWarning
// #define BMR464XXX8_StatusInput_bIInOCWarning        PMBusSpec_StatusInput_bIInOCWarning
// #define BMR464XXX8_StatusInput_bIInOCFault          PMBusSpec_StatusInput_bIInOCFault
// #define BMR464XXX8_StatusInput_bUnitOffForLowInputVoltage  PMBusSpec_StatusInput_bUnitOffForLowInputVoltage
#define BMR464XXX8_StatusInput_bVInUVFault          PMBusSpec_StatusInput_bVInUVFault
#define BMR464XXX8_StatusInput_bVInUVWarning        PMBusSpec_StatusInput_bVInUVWarning
#define BMR464XXX8_StatusInput_bVInOVWarning        PMBusSpec_StatusInput_bVInOVWarning
#define BMR464XXX8_StatusInput_bVInOVFault          PMBusSpec_StatusInput_bVInOVFault

// #define BMR464XXX8_StatusTemperature_bReserved0     PMBusSpec_StatusTemperature_bReserved0
// #define BMR464XXX8_StatusTemperature_bReserved1     PMBusSpec_StatusTemperature_bReserved1
// #define BMR464XXX8_StatusTemperature_bReserved2     PMBusSpec_StatusTemperature_bReserved2
// #define BMR464XXX8_StatusTemperature_bReserved3     PMBusSpec_StatusTemperature_bReserved3
#define BMR464XXX8_StatusTemperature_bUTFault       PMBusSpec_StatusTemperature_bUTFault
#define BMR464XXX8_StatusTemperature_bUTWarning     PMBusSpec_StatusTemperature_bUTWarning
#define BMR464XXX8_StatusTemperature_bOTWarning     PMBusSpec_StatusTemperature_bOTWarning
#define BMR464XXX8_StatusTemperature_bOTFault       PMBusSpec_StatusTemperature_bOTFault

#define BMR464XXX8_StatusCML_bOtherMemoryOrLogicFault       PMBusSpec_StatusCML_bOtherMemoryOrLogicFault
#define BMR464XXX8_StatusCML_bOtherCommunicationFault       PMBusSpec_StatusCML_bOtherCommunicationFault
// #define BMR464XXX8_StatusCML_bReserved2                     PMBusSpec_StatusCML_bReserved2
#define BMR464XXX8_StatusCML_bProcessorFaultDetected        PMBusSpec_StatusCML_bProcessorFaultDetected
#define BMR464XXX8_StatusCML_bMemoryFaultDetected           PMBusSpec_StatusCML_bMemoryFaultDetected
#define BMR464XXX8_StatusCML_bPacketErrorCheckFailed        PMBusSpec_StatusCML_bPacketErrorCheckFailed
#define BMR464XXX8_StatusCML_bInvalidOrUnsupportedData      PMBusSpec_StatusCML_bInvalidOrUnsupportedData
#define BMR464XXX8_StatusCML_bInvalidOrUnsupportedCommand   PMBusSpec_StatusCML_bInvalidOrUnsupportedCommand


#endif
