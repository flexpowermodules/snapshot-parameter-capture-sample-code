// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#ifndef BMR462_STATUSBITMASKS_H
#define BMR462_STATUSBITMASKS_H

#include "PMBusSpec_StatusBitmasks.h"
#include "POL2008E_StatusMFRBitmasks.h"

#define BMR462_StatusMFR_bVMONOVFault              POL2008E_StatusMFR_bVMONOVFault            
#define BMR462_StatusMFR_bVMONUVFault              POL2008E_StatusMFR_bVMONUVFault            
#define BMR462_StatusMFR_bReserved2                POL2008E_StatusMFR_bReserved2              
#define BMR462_StatusMFR_bLossOfExternalSyncFault  POL2008E_StatusMFR_bLossOfExternalSyncFault
#define BMR462_StatusMFR_bVMONOVWarning            POL2008E_StatusMFR_bVMONOVWarning          
#define BMR462_StatusMFR_bVMONUVWarning            POL2008E_StatusMFR_bVMONUVWarning          
#define BMR462_StatusMFR_bReserved6                POL2008E_StatusMFR_bReserved6              
#define BMR462_StatusMFR_bReserved7                POL2008E_StatusMFR_bReserved7              

// #define BMR462_StatusVOut_bVOutTrackingError    PMBusSpec_StatusVOut_bVOutTrackingError 
#define BMR462_StatusVOut_bTOffMaxWarning       PMBusSpec_StatusVOut_bTOffMaxWarning    
#define BMR462_StatusVOut_bTOnMaxFault          PMBusSpec_StatusVOut_bTOnMaxFault       
#define BMR462_StatusVOut_bVOutMaxWarning       PMBusSpec_StatusVOut_bVOutMaxWarning    
#define BMR462_StatusVOut_bVOutUVFault          PMBusSpec_StatusVOut_bVOutUVFault       
// #define BMR462_StatusVOut_bVOutUVWarning        PMBusSpec_StatusVOut_bVOutUVWarning     
// #define BMR462_StatusVOut_bVOutOVWarning        PMBusSpec_StatusVOut_bVOutOVWarning     
#define BMR462_StatusVOut_bVOutOVFault          PMBusSpec_StatusVOut_bVOutOVFault       

// #define BMR462_StatusIOut_bPOutOPWarning     PMBusSpec_StatusIOut_bPOutOPWarning     
// #define BMR462_StatusIOut_bPOutOPFault          PMBusSpec_StatusIOut_bPOutOPFault       
// #define BMR462_StatusIOut_bInPowerLimitingMode  PMBusSpec_StatusIOut_bInPowerLimitingMode   
// #define BMR462_StatusIOut_bCurrentShareFault    PMBusSpec_StatusIOut_bCurrentShareFault 
#define BMR462_StatusIOut_bIOutUCFault          PMBusSpec_StatusIOut_bIOutUCFault       
// #define BMR462_StatusIOut_bIOutOCWarning     PMBusSpec_StatusIOut_bIOutOCWarning     
// #define BMR462_StatusIOut_bIOutOCLVFault     PMBusSpec_StatusIOut_bIOutOCLVFault     
#define BMR462_StatusIOut_bIOutOCFault          PMBusSpec_StatusIOut_bIOutOCFault       

// #define BMR462_StatusInput_bPInOPWarning        PMBusSpec_StatusInput_bPInOPWarning         
// #define BMR462_StatusInput_bIInOCWarning        PMBusSpec_StatusInput_bIInOCWarning         
// #define BMR462_StatusInput_bIInOCFault          PMBusSpec_StatusInput_bIInOCFault           
// #define BMR462_StatusInput_bUnitOffForLowInputVoltage  PMBusSpec_StatusInput_bUnitOffForLowInputVoltage    
#define BMR462_StatusInput_bVInUVFault          PMBusSpec_StatusInput_bVInUVFault           
#define BMR462_StatusInput_bVInUVWarning        PMBusSpec_StatusInput_bVInUVWarning         
#define BMR462_StatusInput_bVInOVWarning        PMBusSpec_StatusInput_bVInOVWarning         
#define BMR462_StatusInput_bVInOVFault          PMBusSpec_StatusInput_bVInOVFault           

// #define BMR462_StatusTemperature_bReserved0     PMBusSpec_StatusTemperature_bReserved0  
// #define BMR462_StatusTemperature_bReserved1     PMBusSpec_StatusTemperature_bReserved1  
// #define BMR462_StatusTemperature_bReserved2     PMBusSpec_StatusTemperature_bReserved2  
// #define BMR462_StatusTemperature_bReserved3     PMBusSpec_StatusTemperature_bReserved3  
#define BMR462_StatusTemperature_bUTFault       PMBusSpec_StatusTemperature_bUTFault        
#define BMR462_StatusTemperature_bUTWarning     PMBusSpec_StatusTemperature_bUTWarning  
#define BMR462_StatusTemperature_bOTWarning     PMBusSpec_StatusTemperature_bOTWarning  
#define BMR462_StatusTemperature_bOTFault       PMBusSpec_StatusTemperature_bOTFault        

#define BMR462_StatusCML_bOtherMemoryOrLogicFault       PMBusSpec_StatusCML_bOtherMemoryOrLogicFault        
#define BMR462_StatusCML_bOtherCommunicationFault       PMBusSpec_StatusCML_bOtherCommunicationFault        
// #define BMR462_StatusCML_bReserved2                     PMBusSpec_StatusCML_bReserved2                  
#define BMR462_StatusCML_bProcessorFaultDetected        PMBusSpec_StatusCML_bProcessorFaultDetected     
#define BMR462_StatusCML_bMemoryFaultDetected           PMBusSpec_StatusCML_bMemoryFaultDetected            
#define BMR462_StatusCML_bPacketErrorCheckFailed        PMBusSpec_StatusCML_bPacketErrorCheckFailed     
#define BMR462_StatusCML_bInvalidOrUnsupportedData      PMBusSpec_StatusCML_bInvalidOrUnsupportedData       
#define BMR462_StatusCML_bInvalidOrUnsupportedCommand   PMBusSpec_StatusCML_bInvalidOrUnsupportedCommand    


#endif
