//StatusBitMasks_PMBus.h
// 
// This is a list of all Status bitfields as documented in the PMBus specification.
// This list does not guarantee that an individual status byte is supported in an individual Ericsson Power module.
// 

// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef PMBUSSPEC_STATUSBITMASKS_H
#define PMBUSSPEC_STATUSBITMASKS_H

#define PMBusSpec_StatusVOut_bVOutTrackingError 0x01
#define PMBusSpec_StatusVOut_bTOffMaxWarning    0x02
#define PMBusSpec_StatusVOut_bTOnMaxFault       0x04
#define PMBusSpec_StatusVOut_bVOutMaxWarning    0x08
#define PMBusSpec_StatusVOut_bVOutUVFault       0x10
#define PMBusSpec_StatusVOut_bVOutUVWarning     0x20
#define PMBusSpec_StatusVOut_bVOutOVWarning     0x40
#define PMBusSpec_StatusVOut_bVOutOVFault       0x80

#define PMBusSpec_StatusIOut_bPOutOPWarning     0x01
#define PMBusSpec_StatusIOut_bPOutOPFault       0x02
#define PMBusSpec_StatusIOut_bInPowerLimitingMode   0x04
#define PMBusSpec_StatusIOut_bCurrentShareFault 0x08
#define PMBusSpec_StatusIOut_bIOutUCFault       0x10
#define PMBusSpec_StatusIOut_bIOutOCWarning     0x20
#define PMBusSpec_StatusIOut_bIOutOCLVFault     0x40
#define PMBusSpec_StatusIOut_bIOutOCFault       0x80

#define PMBusSpec_StatusInput_bPInOPWarning         0x01
#define PMBusSpec_StatusInput_bIInOCWarning         0x02
#define PMBusSpec_StatusInput_bIInOCFault           0x04
#define PMBusSpec_StatusInput_bUnitOffForLowInputVoltage    0x08
#define PMBusSpec_StatusInput_bVInUVFault           0x10
#define PMBusSpec_StatusInput_bVInUVWarning         0x20
#define PMBusSpec_StatusInput_bVInOVWarning         0x40
#define PMBusSpec_StatusInput_bVInOVFault           0x80

#define PMBusSpec_StatusTemperature_bReserved0  0x01
#define PMBusSpec_StatusTemperature_bReserved1  0x02
#define PMBusSpec_StatusTemperature_bReserved2  0x04
#define PMBusSpec_StatusTemperature_bReserved3  0x08
#define PMBusSpec_StatusTemperature_bUTFault    0x10
#define PMBusSpec_StatusTemperature_bUTWarning  0x20
#define PMBusSpec_StatusTemperature_bOTWarning  0x40
#define PMBusSpec_StatusTemperature_bOTFault    0x80

#define PMBusSpec_StatusCML_bOtherMemoryOrLogicFault     0x01
#define PMBusSpec_StatusCML_bOtherCommunicationFault     0x02
#define PMBusSpec_StatusCML_bReserved2                   0x04
#define PMBusSpec_StatusCML_bProcessorFaultDetected      0x08
#define PMBusSpec_StatusCML_bMemoryFaultDetected         0x10
#define PMBusSpec_StatusCML_bPacketErrorCheckFailed      0x20
#define PMBusSpec_StatusCML_bInvalidOrUnsupportedData    0x40
#define PMBusSpec_StatusCML_bInvalidOrUnsupportedCommand 0x80

#endif

