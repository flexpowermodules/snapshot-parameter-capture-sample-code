SOURCES=$(wildcard src/**/*.c src/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

DEMO_SRC=$(wildcard demo/*.c)
DEMO=$(patsubst %.c,%,$(DEMO_SRC))

#CFLAGS=-std=C99

all: demo

.PHONY: demo
demo: lib 
	$(CC) $(DEMO_SRC) $(OBJECTS) -o $(DEMO)

lib: $(OBJECTS)

clean:
	rm -rf build $(OBJECTS) $(DEMO)
	find . -name "*.gc*" -exec rm {} \;
	rm -rf `find . -name "*.dSYM" -print`
