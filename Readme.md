#Snapshot Parameter Capture - Sample Code

This is a piece of sample code to demonstrate the snapshot function available on the BMR462-465 Ericsson PoL Digital Power Modules. The feature allows one to retrieve fault information after a fault event - This sample code is intended to complement App Note 320 - Snapshot Parameter Capture, which can be found on the Ericsson Power Modules website: http://www.ericsson.com/ourportfolio/products/power-modules 

##Building steps

To build the demo with gcc, just cd to the directory containing this readme and do the following:

$ make 

$ ./demo/SnapshotDemo


##License

Copyright © Ericsson AB 2014

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a
compiled binary, for any purpose, commercial or non-commercial, and
by any means.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
